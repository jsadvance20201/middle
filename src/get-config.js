const axios = require('axios')

let configCash = {

}

const fetchConfig = async (url, cashMinuts, tag) => {
    const answer = await axios.get(url + tag);

    configCash[tag] = {
        data: answer.data,
        ts: new Date().valueOf() + cashMinuts * 60 * 1000
    }
}

const getConfig = async (configUrl, cashMinuts = 1, tag) => {
    if (!configCash[tag] || configCash[tag].ts < new Date().valueOf()) {
        await fetchConfig(configUrl, cashMinuts, tag);
    }

    return configCash[tag].data;
}

module.exports = getConfig
