const express = require('express')
const path = require('node:path')
const { applyHbs } = require('@ijl/templates')

const app = express()

const isProd = process.env.NODE_ENV === 'production'

applyHbs(app)

require('./copy-files')
const rc = require('../.serverrc.json')
const getConfig = require('./get-config')

const getSubdomain = (hostname) => hostname.split('.')[0]

const start = async () => {
  app.use('/static', async (req, res) => {
    const { baseUrl } = await getConfig(
      rc.configUrl,
      rc.cashMinuts,
      getSubdomain(request.hostname),
    )
    res.redirect(`${baseUrl}${req.url}`)
  })

  app.get(
    [
      '/site.webmanifest',
      '/favicon-16x16.png',
      '/favicon-32x32.png',
      '/android-chrome-512x512.png',
      '/android-chrome-192x192.png',
      '/apple-touch-icon.png',
      '/favicon.ico',
      '/site.webmanifest',
    ],
    (request, responce) => {
      responce.sendFile(path.join(__dirname, 'public', request.originalUrl))
    },
  )

  app.use('/', async (request, response) => {
    let settings = require('./settings')
    // if (isProd) {
    settings = await getConfig(
      rc.configUrl,
      rc.cashMinuts,
      getSubdomain(request.hostname),
    )
    // }

    response.render('index.hbs', settings)
  })

  const port = isProd ? rc.port : 80

  app.listen(port, console.log('🛠', 'middle is ready on port:', port))
}

module.exports = start
